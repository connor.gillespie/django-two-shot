from django.contrib.auth.views import LoginView, LogoutView
from django.urls import path
from receipts.views import (
    list_receipts,
    create_receipts,
    list_accounts,
    list_categories,
    create_categories,
    create_accounts,
)


urlpatterns = [
    path("", list_receipts, name="home"),
    path("create/", create_receipts, name="create_receipt"),
    path("accounts/", list_accounts, name="list_accounts"),
    path("categories/", list_categories, name="list_categories"),
    path("categories/create/", create_categories, name="create_category"),
    path("accounts/create/", create_accounts, name="create_account"),
]
